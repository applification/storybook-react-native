/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved, import/extensions */

import React from "react";
import { Text } from "react-native";

import { storiesOf } from "@storybook/react-native";
import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";

import Button from "../../components/Button";
import IconButton from "../../components/IconButton";
import CenterView from "../../components/CenterView";
import Welcome from "../../components/Welcome";
import CardImage from "../../components/card";

storiesOf("Welcome", module).add("to Storybook", () => (
  <Welcome showApp={linkTo("Button")} />
));

storiesOf("Button", module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add("with text", () => (
    <Button onPress={action("clicked-text")}>
      <Text>Hello Button</Text>
    </Button>
  ))
  .add("with some emoji", () => (
    <Button onPress={action("clicked-emoji")}>
      <Text>😀 😎 👍 💯</Text>
    </Button>
  ));

storiesOf("IconButton", module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add("Back Button", () => (
    <IconButton
      theme={"success"}
      disabled={false}
      icon="arrow-back"
      onPress={action("clicked back button")}
    >
      Back
    </IconButton>
  ))
  .add("Settings Button", () => (
    <IconButton
      theme={"success"}
      disabled={false}
      icon="cog"
      onPress={action("clicked settings button")}
    >
      Settings
    </IconButton>
  ))
  .add("Settings Button Disabled", () => (
    <IconButton
      theme={"dark"}
      disabled={true}
      icon="cog"
      onPress={action("clicked settings button")}
    >
      Settings
    </IconButton>
  ));

storiesOf("Card", module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add("Card Image", () => (
    <CardImage
      title="Card Header"
      subTitle="Sub Title"
      likes="24"
      comments="11"
      ago="9h ago"
    />
  ));
