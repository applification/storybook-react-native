import React, { PropTypes } from "react";
import { Container, Content, Button, Icon, Text } from "native-base";

export default function IconButton(props) {
  let success = false;
  if (props.theme === "success") {
    success = true;
  }

  let dark = false;
  if (props.theme === "dark") {
    dark = true;
  }

  return (
    <Button
      success={success}
      dark={dark}
      disabled={props.disabled}
      iconLeft
      onPress={props.onPress}
    >
      <Icon name={props.icon} />
      <Text>{props.children}</Text>
    </Button>
  );
}

IconButton.defaultProps = {
  children: null,
  onPress: () => {}
};

IconButton.propTypes = {
  children: PropTypes.node,
  onPress: PropTypes.func
};
