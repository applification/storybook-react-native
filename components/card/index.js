import React, { Component, PropTypes } from "react";
import { Image } from "react-native";
import {
  Container,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body
} from "native-base";
export default class CardImage extends Component {
  render() {
    const { title, subTitle, likes, comments, ago } = this.props;
    return (
      <Container>
        <Content>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail source={require("../../assets/thumbnail.png")} />
                <Body>
                  <Text>{title}</Text>
                  <Text note>{subTitle}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image
                style={{ width: "100%" }}
                source={require("../../assets/cardShowcase.png")}
              />
            </CardItem>
            <CardItem>
              <Button transparent>
                <Icon active name="thumbs-up" />
                <Text>{likes} Likes</Text>
              </Button>
              <Button transparent>
                <Icon active name="chatbubbles" />
                <Text>{comments} Comments</Text>
              </Button>
              <Text>{ago}</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
